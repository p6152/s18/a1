console.log(`Hello World`)


	let trainer = {
		name: ``,
		age: 0,
		pokemon: [],
		friends: {
			properties: []
		},
		talk: function(opponent){       
		            console.log(`Pikachu! I choose you!`)
		        }
	};

	console.log(trainer.name)
	console.log(trainer.age)
	console.log(trainer.pokemon)
	console.log(trainer.friends.properties)
	console.log(trainer.talk)

	trainer.talk();

	function Pokemon (name, level, health, attack) {
		this.name = name;
		this.level = level;
		this.health = health;
		this.attack = attack;

		this.tackle = function(opponent){
					this.health -= opponent.attack; 
					if(this.health <= 0){
						return this.faint();
					}
				},
		this.faint = function(opponent){
					console.log(`${this.name} has fainted`);
				}
	};

	let pikachu = new Pokemon(`Pikachu`, 5, 100, 100);
	let snorlax = new Pokemon(`Snorlax`, 7, 100, 40);
	let charizard = new Pokemon(`Charizard`, 3, 100, 30);
	let lucario = new Pokemon(`Lucario`, 1, 100, 20);

	pikachu.tackle(snorlax);
	pikachu.tackle(snorlax);
	pikachu.tackle(snorlax);
	console.log(pikachu)
	console.log(snorlax)



	
